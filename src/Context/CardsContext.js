import { createContext, useState } from "react";

export const CardsContext = createContext();

export const CardsProvider = ({ children}) => {
 
  const [query, setQuery] = useState("");
  const [filteredPosts, setFilteredPost] = useState();

  const submit = (e) => {
    e.preventDefault();
    setQuery('')
    fetch(`https://imdb-api.com/en/API/SearchSeries/k_hq67qz3p/${query}`)
    .then((rez) => rez.json())
    .then((d) => setFilteredPost(d.results));  
  };


  const obj = {
    query,
    setQuery,
    submit,
    filteredPosts,
  };

  return <CardsContext.Provider value={obj}>{children}</CardsContext.Provider>;
};

