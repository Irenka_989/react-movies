import Home from '../Pages/Home'
import Details from "../Pages/Details"


const routes = {
    "/" : () => <Home/>,
    "/details/:id" : ({id}) => <Details id={id}/>
}

export default routes