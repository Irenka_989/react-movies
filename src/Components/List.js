import React, { useContext } from "react";
import Card from "../Components/Card";
import { CardsContext } from "../Context/CardsContext";

const List = () => {
  const { filteredPosts } = useContext(CardsContext);
  return (
    <div className="List">
      {filteredPosts && filteredPosts.map((post) => (
        <Card key={post.id} data={post} />
      ))}
    </div>
  );
};

export default List;
