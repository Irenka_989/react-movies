import React, { useContext } from "react";
import { CardsContext } from "../Context/CardsContext";

const Search = () => {
  const { query, setQuery, submit } = useContext(CardsContext);
  return (
    <div className="Search">
      <h1>Search series</h1>
      <form onSubmit={submit}>
        <input
          type="search"
          value={query}
          onChange={(e) => setQuery(e.target.value)}
        />
        <button type="submit">
          <i className="fas fa-search"></i>
        </button>
      </form>
    </div>
  );
};

export default Search;
