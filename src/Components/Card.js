import React from "react";
import { navigate } from "hookrouter";

const Card = ({ data }) => {
  const { id, title, image, description } = data;
  const redirect = () => {
    navigate(`/details/${id}`);
  };
  return (
    <div
      onClick={redirect}
      className="Card"
    >
      <h3>{title}</h3>
      <img src={image} alt="movie-img" />
      <p>{description}</p>
      <p>id: {id}</p>
    </div>
  );
};

export default Card;
