import React, { useEffect, useState } from "react";

const Details = ({ id }) => {
  const [movie, setMovie] = useState();

  useEffect(() => {
    fetch(
      `https://imdb-api.com/en/API/Title/k_hq67qz3p/${id}/FullActor,Ratings,`
    )
      .then((rez) => rez.json())
      .then((data) => setMovie(data));
  });

  return (
    <div className="Details">
      {movie ? (
        <div>
          <h3>{movie.title}</h3>
          <img className="img-details" src={movie.image} alt="details" />
          <ul>
            {" "}
            Actors:
            {movie.actorList.map((actor) => (
              <li key={actor.id}>{actor.name} as: {actor.asCharacter}</li>
            ))}
          </ul>
          <h6>Raitings:</h6>
          <p>imDb: {movie.ratings.imdb}</p>
          <p>the movie db: {movie.ratings.theMovieDb}</p>
          <p>tv com: {movie.ratings.tv_com}</p>
          <p>filmaffinity: {movie.ratings.filAffinity}</p>
        </div>
      ) : (
        <div>Loading...</div>
      )}
    </div>
  );
};

export default Details;
