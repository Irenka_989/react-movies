import React  from "react";
import Search from "../Components/Search";
import List from "../Components/List";
import { CardsProvider } from "../Context/CardsContext";

const Home = () => {

  return (
    <div className="Home">
      <CardsProvider>
        <Search />
        <List />
      </CardsProvider>
    </div>
  );
};

export default Home;
