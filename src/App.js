
import './App.css';
import routes from './Router/Routes';
import {useRoutes} from 'hookrouter'

function App() {

  const routeResults = useRoutes(routes)
  return (
    <div className="App">
      {routeResults || <div className="error">ERROR 404</div>}
    </div>
  );
}

export default App;
